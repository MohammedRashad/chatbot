from llama_index.core.llms import ChatMessage
from rag_v1 import rag_pipeline
from config import *
from utils import *
import streamlit as st

st.set_page_config(page_title="Chat with the Terraform docs, powered by Stakapk", layout="centered", initial_sidebar_state="auto", menu_items=None)
st.title("Chat with the Terraform docs, powered by Stakapk")
         
if "messages" not in st.session_state.keys(): # Initialize the chat messages history
    st.session_state.messages = [
        {"role": "assistant", "content": "I'm a Senior DevOps Enigneer, Ask me a question about Terraform!"}
    ]

@st.cache_resource(show_spinner=False)
def load_data():
    with st.spinner(text="Loading our index, This should take 1 minute..."):
        query_engine, llm, vector_index = rag_pipeline(setup_mode=False)

        return query_engine, llm, vector_index

query_engine, llm, index = load_data()

if "chat_engine" not in st.session_state.keys(): # Initialize the chat engine
        st.session_state.chat_engine = index.as_chat_engine(chat_mode="condense_question", verbose=True)

if prompt := st.chat_input("Your question"): # Prompt for user input and save to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})

for message in st.session_state.messages: # Display the prior chat messages
    with st.chat_message(message["role"]):
        st.write(message["content"])

# If last message is not from assistant, generate a new response
if st.session_state.messages[-1]["role"] != "assistant":
    with st.chat_message("assistant"):
        with st.spinner("Thinking..."):
            response_intermediate = st.session_state.chat_engine.chat(MAIN_PROMPT + "\n\n"+prompt)

            print("--------")
            print(response_intermediate)
            print("--------")
            messages = [
                ChatMessage(
                    role="system", content=AUGMENTATION_PROMPT
                ),
                ChatMessage(role="user", content=response_intermediate),
            ]
            response = llm.chat(messages)
            print(response)


            st.write(response)
            message = {"role": "assistant", "content": response}
            st.session_state.messages.append(message) # Add response to message history
            log_message(prompt, "response : " +response_intermediate +" llama_imporved : " + response)