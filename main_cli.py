import rag_v1
from config import *
from utils import *
if __name__ == '__main__':
  #  rag_v1.rag_pipeline(setup_mode=True)
    query_engine, llm, vector_index= rag_v1.rag_pipeline(setup_mode=False)

    while True:
        query = input("Ask a question: ")
        response_intermediate = query_engine.query(MAIN_PROMPT+ "\n\n" + query)
        print(response_intermediate)
        print("--------")
        messages = [
            rag_v1.ChatMessage(
                role="system", content=rag_v1.AUGMENTATION_PROMPT
            ),
            rag_v1.ChatMessage(role="user", content=query),
        ]
        response = llm.chat(messages)
        print(response)
        log_message(query, "response : " +response_intermediate +" llama_imporved : " + response)