from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from rag_v1 import rag_pipeline
from llama_index.core.llms import ChatMessage
from config import *
from utils import *

# Sample Query : 
#curl -X POST http://127.0.0.1:8000/answer -H "Content-Type: application/json" -d "{\"query\":\"What is Terraform?\"}"

app = FastAPI()
query_engine, llm, vector_index = rag_pipeline(setup_mode=False)

class Question(BaseModel):
    query: str

@app.post("/answer")
async def answer_question(question: Question):
    print("x")
    try:
        print("y")
        print(question.query)
        response_intermediate = query_engine.query(question.query)
        print('z')
        messages = [
            ChatMessage(
                role="system", content=AUGMENTATION_PROMPT
            ),
            ChatMessage(role="user", content=response_intermediate),
        ]
        response = llm.chat(messages)
        log_message(question.query, "response : " +response_intermediate.response +" llama_imporved : " + str(response))

        print("output")
        print(question.query)
        print(response)
        return {"question": question.query, "answer": response}
    except Exception as e:
        # print(str(e))
        # return {"error": str(e)}
        raise HTTPException(status_code=500, detail=str(e))

