export const defaultConfig = {
    url: "https://developer.hashicorp.com/terraform/",
    match: "https://developer.hashicorp.com/terraform/**",
    maxPagesToCrawl: 2000,
    outputFileName: "output.json",
    maxTokens: 2000000,
};
//# sourceMappingURL=config.js.map