import { Config } from "./src/config";

export const defaultConfig: Config = {
  url: "https://developer.hashicorp.com/terraform/",
  match: "https://developer.hashicorp.com/terraform/**",
  maxPagesToCrawl: 2000,
  outputFileName: "output.json",
  maxTokens: 2000000,
};
