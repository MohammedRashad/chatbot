import json
import os

# Read the JSON file
json_file_path = 'output-1.json'
with open(json_file_path, 'r') as file:
    data = json.load(file)  # Assuming the top-level JSON structure is an array

# Specify the folder where you want to save the text files
output_folder = 'output_text_files'
if not os.path.exists(output_folder):
    os.makedirs(output_folder)  # Create the folder if it doesn't exist

# Function to sanitize file names
def sanitize_filename(title):
    for char in ['/', '\\', ':', '*', '?', '"', '<', '>', '|']:
        title = title.replace(char, '-')
    return title

# Iterate through the array and generate text files
for item in data:
    # Use the sanitized 'title' as the filename
    sanitized_title = sanitize_filename(item.get('title', 'Untitled'))
    file_name = f"{sanitized_title}.txt"
    file_path = os.path.join(output_folder, file_name)
    
    # Write the 'html' content to the text file
    with open(file_path, 'w', encoding='utf-8') as text_file:
        content = item.get('html', '')  # Default to empty string if 'html' key doesn't exist
        text_file.write(content)

print(f"Finished generating text files in the folder: {output_folder}")
