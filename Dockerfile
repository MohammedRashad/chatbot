FROM python:3.11.8-slim
WORKDIR /ai-bot
COPY ./requirements.txt /ai-bot/requirements.txt
RUN pip install --no-cache-dir -r /ai-bot/requirements.txt
COPY . /ai-bot
EXPOSE 8000
CMD ["uvicorn", "main_api:app", "--host", "0.0.0.0", "--port", "8000"]