# Stakpak Chatbot

### Ways to run

command line version : `python3 main_cli.py`

API version : 

- `python3 -m uvicorn main_api:app --reload`
- `curl -X POST http://127.0.0.1:8000/answer -H "Content-Type: application/json" -d '{"query":"What is Terraform?"}'`

UI version : `python3 -m streamlit run main_ui.py`